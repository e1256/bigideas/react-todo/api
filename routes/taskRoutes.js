
const express = require("express");
const router = express.Router();

//contollers
const taskController = require("./../controllers/taskControllers");

//create new task
router.post("/create", (req, res)=>{
	taskController.createNew(req.body).then(result => res.send(result));
})
//get all tasks
router.get("/all", (req, res) => {
	taskController.getAllTask().then(result => res.send(result));
})
// select task
router.get("/:taskId", (req, res) => {
	taskController.getTask(req.params.taskId).then( result => res.send(result))
})
//archive
router.put('/:taskId/archive', (req, res) => {
	taskController.archiveTask(req.params.taskId, req.body).then( result => res.send(result))
})
//unarchive
router.put('/:taskId/unarchive', (req, res) => {
	taskController.unarchiveTask(req.params.taskId, req.body).then( result => res.send(result))
})
//delete 
router.delete('/:taskId/delete', (req,res) => {
	taskController.deleteTask(req.params.taskId).then(result=> res.send(result))
})
//delete all
router.delete('/deleteAll', (req, res)=> {
	taskController.deleteAll().then(result => res.send(result))
})



module.exports = router;

