
const Task = require("./../models/Task");

module.exports.createNew = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then( (result, error) =>{
		if(error){
			return error
		} else {
			return true
		}
	})
}
module.exports.getAllTask = () => {
	return Task.find({}).then(result => result)
} 
module.exports.getTask = (params) => {
	return Task.findById(params).then( result => result)
}

module.exports.archiveTask = ( params, reqBody) => {
	let updatedTask = {
		isDone: true
	}
	return Task.findByIdAndUpdate(params, updatedTask, {new:true})
	.then((result , error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}
module.exports.unarchiveTask = ( params, reqBody) => {
	let updatedTask = {
		isDone: false
	}
	return Task.findByIdAndUpdate(params, updatedTask, {new:true})
	.then((result , error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}
module.exports.deleteTask = (params) => {
	return Task.findByIdAndDelete(params)
	.then((result, error)=> {
		if(error){
			return false
		} else {
			return true
		}
	})
}
module.exports.deleteAll = () => {
	return Task.deleteMany({}).then(result => result)
} 

