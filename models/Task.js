const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Task name is required"]
		},
		isDone: {
			type: Boolean,
			default: false
		},
	}
);

module.exports = mongoose.model("Task", taskSchema);