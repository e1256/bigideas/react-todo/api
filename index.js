const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000;
const app = express();
const cors = require('cors');

// https://fast-plateau-17170.herokuapp.com/ 

//routes
let taskRoutes = require("./routes/taskRoutes");

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://arthur25:arthurespartinez@cluster0.iymnv.mongodb.net/course_booking?retryWrites=true&w=majority",
	{useNewUrlParser: true, useUnifiedTopology: true}
).then(()=>console.log(`Connected to Database`))
.catch( (error)=> console.log(error))

//routes
app.use("/api/tasks", taskRoutes);

app.listen(PORT, () => console.log(`Server running at port ${PORT}`));